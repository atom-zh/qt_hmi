#ifndef __MAIN_H
#define __MAIN_H

#include <QString>

struct InputArgs {
    char currVersion[16];
    bool isTestVersion;
    bool isForceUpdate;
    QString localSrcPath;

    InputArgs () {
        isTestVersion = false;
        isForceUpdate = false;
    }
};

extern struct InputArgs inputArgs;
extern int hostLanguage;
extern QString updateChannel;
extern int update_rc;

#endif //__MAIN_H
