#ifndef __COMMAND_H__
#define __COMMAND_H__

#include <QObject>
#include <QSslSocket>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTimerEvent>
#include <QBasicTimer>
#include <QThread>
#include <QTcpSocket>
#include <QXmlStreamReader>
#include <QtNetwork/QLocalSocket>
#include <QtNetwork/QAbstractSocket>
#include <QFile>

class Command : public QObject
{
    Q_OBJECT

public:
    Command();
    ~Command();

    virtual bool isConnAlive() = 0;

public slots:
    virtual void slotConnectOperateServer(const char *, int) = 0;
    virtual void slotSendOperateData(QByteArray &) = 0;
    virtual void slotDisconnectOperateServer(void) = 0;
    virtual void slotReceiveData(QByteArray*, unsigned int) = 0;

signals:
    void error(QString);
};

class CommandSock : public Command
{
    Q_OBJECT

public:

    bool isConnAlive();

public slots:
    void slotConnectOperateServer(const char *, int);
    void slotSendOperateData(QByteArray &);
    void slotDisconnectOperateServer(void);
    void slotReceiveData(QByteArray*, unsigned int);
    void slotStateChanged(QAbstractSocket::SocketState);
    void slotConnectError(QAbstractSocket::SocketError);

private:
	QTcpSocket *opClient;
};

#endif 
