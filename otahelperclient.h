#ifndef __NETWORKCLENT_H
#define __NETWORKCLENT_H

#include <QObject>
#include <QSslSocket>
#include <QNetworkReply>
#include <QEventLoop>
#include <QTimerEvent>
#include <QBasicTimer>
#include <QThread>
#include <QTcpSocket>
#include <QXmlStreamReader>
#include <QtNetwork/QLocalSocket>
#include <QtNetwork/QAbstractSocket>
#include <QFile>
#include "command.h"

class Command;
class CommandSock;

enum UpdateChargeReturnCode {
    _REQUEST_FAILED = 0, // server not access or return bad info
    _REQUEST_NO_UPDATES, // no need to update
    _REQUEST_OK          // need to update
};

class OtaHelperCli : public QThread
{
	Q_OBJECT

public :
    enum CommandType { _UNKNOWN_EVT = 0,
                       INSTALL_NOW,
                       BOOK_INSTALL,
                       CHK_POL,
                       DBG_RESET,
                       DEBUG_EXPORT_LOG,
                       UPDATE_SUCCESS,
                       UPDATE_FAIL,
                       POLICY_FAIL,
                       PROGRESS_CHG };

	OtaHelperCli(QObject *parent=0);
	~OtaHelperCli();

	unsigned int CampaignId() { return _campaignId; }
	void setPipePath(QByteArray pipePath) { _pipePath = pipePath; }
	QByteArray getPipePath() { return _pipePath; }
	void setSocketNetPath(QByteArray addrPath, int port) { _addr = addrPath; _port = port; }
	void getSocketNetPath(QByteArray &addr, int *port) { addr = this->_addr; *port = this->_port; }
	int booktime;
	int polcmd;
	int total_percent;
	int current_percent;
	int isClickOK;

signals:
	void popupWindow(int);
	void setProgressValue(int, QString);
	void errStatusChanged(int, QString);

public slots:
	void run() Q_DECL_OVERRIDE;
	void userClicked(int);
	void onCommandError(QString err);

private:
	bool _initConnection();
	int _getRequest(const char * request);
	bool _isConnect;
	bool _isRunning;
	FILE *_fifoFile;
	int _fd;
	QByteArray _pipePath;
	unsigned int _campaignId;
	Command *_opCmd;
	QByteArray _addr;
	int _port;
};

#endif // _NETWORKCLENT_H_
