CONFIG += QT
QT     += core gui widgets network

TARGET = qt_simudisp
HEADERS += \
#   ./singleapplication.h \
#   ./mainwindow.h \
   ./updatedialog.h \
   ./otahelperclient.h \
   ./main.h \
   ./command.h \

SOURCES += \
   ./main.cpp \
#   ./mainwindow.cpp \
   ./updatedialog.cpp \
   ./otahelperclient.cpp \
#   ./singleapplication.cpp \
    ./command.cpp

#--- your libs
#--- your includes
LIBS +=
linux-g++: INCLUDEPATH +=$PWD \
   $PWD/. \
