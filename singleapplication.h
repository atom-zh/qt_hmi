#ifndef SINGLEAPPLICATION_H
#define SINGLEAPPLICATION_H

#include <QObject>
#include <QApplication>
#include <QtNetwork/QLocalSocket>
#include <QWidget>
#include <QThread>

class LocalSocketHandleThread : public QThread
{
	Q_OBJECT
public:
	LocalSocketHandleThread (QLocalSocket &sock);
	~LocalSocketHandleThread();

	void run();

private slots:
	void onReadyRead();
	void onError(QLocalSocket::LocalSocketError);

private:
	int writeData(QByteArray &data);
	QLocalSocket *_client;
};

class SingleApplication : public QApplication {
        Q_OBJECT
public:
    SingleApplication(int argc, char **argv);
    bool isRunning();
    QWidget *w;

private:
    void _initLocalConnection();
    void _activateWindow();
    bool _isRunning;
	QLocalSocket *_localSocket;
	QByteArray _pipeName;
};

#endif // SINGLEAPPLICATION_H
