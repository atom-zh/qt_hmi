#ifndef __UPDATE_DIALOG_H
#define __UPDATE_DIALOG_H

#include <QtWidgets>
#include "otahelperclient.h"

class UpdateDialog : public QWidget
{
	Q_OBJECT
public:
	enum { Stopped = 0, Started };

	UpdateDialog (OtaHelperCli *updateCli, QWidget *parent = NULL);
	~UpdateDialog();

signals:
	void userClicked(int);

public slots:
	void onQuitButtonClicked();
	void onConfirmUpdate();
	void onPopupWindowRequested(int windowId);
	void onProgressChanged(int value, QString dnSpeed);
	void onErrorStatusChanged(int errCode, QString errString);
	void onBookInstallClicked();
	void onBookOkClicked();

private:
	int updateStatus;
	QLabel *errLabel;
	QLabel *statusLabel;
	QTextEdit *infoView;
	QSpinBox *selectMinutesBox;
	QPushButton *updateButton, *quitButton, *bookInstallButton, *bookOkButton;
	QProgressBar *progressBar, *subProgressBar;
	QLabel *speedLabel;
	QLabel *percentageLabel, *subPercentageLabel;
	bool isConfirmedByUser;
	OtaHelperCli *updateCli;
};

#endif
